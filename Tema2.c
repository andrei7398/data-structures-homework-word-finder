#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define BUFLEN 1024
#define ELEMENT_TREE_LENGTH 3

#include "AVLTree.h"

typedef struct Range{
	int *index;
	int size;
	int capacity;
}Range;

void printFile(char* fileName){
	if(fileName == NULL) return;
	FILE * file = fopen(fileName,"r");
	if (file == NULL) return;
	char *buf = (char*) malloc(BUFLEN+1);
	while(fgets(buf,BUFLEN,file) != NULL){
		printf("%s",buf);
	}
	printf("\n");
	free(buf);
	fclose(file);
}

void printWordsInRangeFromFile(Range* range, char* fileName){
	if(fileName == NULL || range == NULL) return;
	FILE * file = fopen(fileName,"r");
	if (file == NULL) return;
	char *buf = (char*) malloc(BUFLEN+1);
	for(int i = 0; i < range->size;i++){
		fseek(file,range->index[i],SEEK_SET);
		if(fgets(buf,BUFLEN,file) != NULL){
			char* token = strtok(buf," .,\n");
			printf("%d. %s:%d\n",i+1, token, range->index[i]);
		}
	}
	printf("\n");
	free(buf);
	fclose(file);
}

void printTreeInOrderHelper(TTree* tree, TreeNode* node){
	if(node != NULL){
		printTreeInOrderHelper(tree, node->lt);
		TreeNode* begin = node;
		TreeNode* end = node->end->next;
		while(begin != end){
			printf("%d:%s  ",*((int*)begin->info),((char*)begin->elem));
			begin = begin->next;
		}
		printTreeInOrderHelper(tree, node->rt);
	}
}

void printTreeInOrder(TTree* tree){
	if(tree == NULL) return;
	printTreeInOrderHelper(tree, tree->root);
}


void* createStrElement(void* str){
	// TODO: Cerinta 2
	/* Allocate a string element to store in the tree.
	 */
	return NULL;
}

void destroyStrElement(void* elem){
	// TODO: Cerinta 2
	/* Free a string element.
	 */
}


void* createIndexInfo(void* index){
	// TODO: Cerinta 2
	/* Allocate space to store a new index
	 */
	return NULL;
}

void destroyIndexInfo(void* index){
	// TODO: Cerinta 2
	/* Free storage space for the index.
	 */
}

int compareStrElem(void* str1, void* str2){
	// TODO: Cerinta 2
	/*  0  means strings are equal 
	 * -1  means str1 < str2
	 *  1  means str1 > str2
	 */
	return 0xFFFFFFFF;
}


TTree* buildTreeFromFile(char* fileName){
	// TODO: Cerinta 2
	/* Parse the text file keeping track of indices!
	 * Words separators are the following: " :.,\r\n"
	 * At each step insert the <token, index> pair in the tree.
	 */
	return NULL;
}


Range* singleKeyRangeQuery(TTree* tree, char* q){
	// TODO: Cerinta 3
	/* Get the words indices begining with the patern specified by q
	 * How can do you traverse the tree?
	 * There might be duplicates, can you make use of the tree duplicate lists?
	 */
	return NULL;
}

Range* multiKeyRangeQuery(TTree* tree, char* q, char* p){
	// TODO: Cerinta 4
	/* Get the words indices begining with the any patern 
	 * between q and p (inclusive).
	 */
	return NULL;
}


int main(void) {

	printf("The text file:\n");
	printFile("text.txt");

	TTree* tree = buildTreeFromFile("text.txt");
	printf("Tree In Order:\n");
	printTreeInOrder(tree);
	printf("\n\n");

	printf("Single search:\n");
	Range *range = singleKeyRangeQuery(tree,"v");
	printWordsInRangeFromFile(range,"text.txt");

	printf("Multi search:\n");
	Range *range2 = multiKeyRangeQuery(tree,"j","pr");
	printWordsInRangeFromFile(range2,"text.txt");

	if(range != NULL) free(range->index);
	free(range);

	if(range2 != NULL) free(range2->index);
	free(range2);

	destroyTree(tree);
	return 0;
}





