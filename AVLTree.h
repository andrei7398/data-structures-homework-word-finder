// Iulian-Andrei Sfetcu - 312CD - First year - Second Semester

#ifndef AVLTREE_H_
#define AVLTREE_H_

#include <stdlib.h>

#define MAX(a, b) (((a) >= (b))?(a):(b))
#define HEIGHT(x) ((x)?((x)->height):(0))

// ----------------------------------------------------------------------------

typedef struct node{
	void* elem;
	void* info;
	struct node *pt;
	struct node *lt;
	struct node *rt;
	struct node* next;
	struct node* prev;
	struct node* end;
	long height;
}TreeNode;


typedef struct TTree{
	TreeNode *root;
	void* (*createElement)(void*);
	void (*destroyElement)(void*);
	void* (*createInfo)(void*);
	void (*destroyInfo)(void*);
	int (*compare)(void*, void*);
	long size;
}TTree;

// ----------------------------------------------------------------------------

TTree* createTree(void* (*createElement)(void*), void (*destroyElement)(void*),
		void* (*createInfo)(void*), void (*destroyInfo)(void*),
		int compare(void*, void*)) {
	
	// Dynamic memory allocation for new tree & check allocation
	TTree *tree = (TTree*)malloc(sizeof(TTree));
	if (!tree)
		return NULL;

	// Set tree functions,root and size
	tree->createElement = createElement;
	tree->createInfo = createInfo;

	tree->destroyElement = destroyElement;
	tree->destroyInfo = destroyInfo;

	tree->compare = compare;

	tree->root = NULL;

	tree->size = 0;

	return tree;
}


TreeNode* createTreeNode(TTree *tree, void* value, void* info) {	
	
	// Dynamic memory allocation for new node & check allocation
	TreeNode *newnode = (TreeNode*)malloc(sizeof(TreeNode));

	if (!newnode)
		return NULL;

	// Set node's links, functions and height
	newnode->elem = tree->createElement(value);
	newnode->info = tree->createInfo(info);

	// Tree links
	newnode->lt = NULL;
	newnode->rt = NULL;
	newnode->pt = NULL;

	// List links
	newnode->prev = NULL;
	newnode->next = NULL;
	newnode->end = NULL;

	// Node height in tree
	newnode->height = 1;

	return newnode;
}


void destroyTreeNode(TTree *tree, TreeNode* node) {

	if (!node)
		return;

	// Destroying void elements using tree functions
	tree->destroyElement(node->elem);
	tree->destroyInfo(node->info);

	// Set tree links NULL
	node->lt = NULL;
	node->rt = NULL;
	node->pt = NULL;

	// Set list links NULL
	node->prev = NULL;
	node->next = NULL;
	node->end = NULL;

	// Set node height 0
	node->height = 0;

	free(node);

    return;
}


int isEmpty(TTree *tree) {

	// If tree is NULL, he is not empty, he doesn't exist
	if (tree == NULL)
		return 0;

	// Empty case 
	if (tree->size == 0 && tree->root == NULL)
		return 1;

	return 0xFFFFFFFF;
}


TreeNode* search(TTree *tree, TreeNode *x, void *elem) {

	// If tree is NULL, it has no nodes to search
	if (!tree)
		return NULL;

	// Loop down to find the needed node
	while (x != NULL) {

		if (tree->compare(x->elem,elem) == 0) {
			return x;
		}
		if (tree->compare(x->elem,elem) < 0) {
			x = x->rt;
			continue;
		}
		if (tree->compare(x->elem,elem) > 0) {
			x = x->lt;
			continue;
		}
	}

	return NULL;
}


TreeNode* minimum(TTree *tree, TreeNode *x) {

	// No minimum node if tree doesn't exist
	if(!tree)
		return NULL;

	// Check if the node is in the tree
	if(search(tree,x,x->elem) == NULL)
		return NULL;

    TreeNode *curr = x;
 
    // Loop down to find the leftmost leaf 
    while (curr->lt != NULL)
        curr = curr->lt;
 		
    return curr;
}


TreeNode* maximum(TTree *tree, TreeNode *x) {

	// No maximum node if tree doesn't exist
	if(!tree)
		return NULL;

	// Check if the node is in the tree
	if(search(tree,x,x->elem) == NULL)
		return NULL;

   	TreeNode *curr = x;
 
   	// Loop down to find the rightmost leaf 
  	while (curr->rt != NULL)
    	curr = curr->rt;

    return curr;
}


TreeNode* successor(TTree* tree, TreeNode* x) {

	if (x == NULL)
		return NULL;

  	if (x->rt != NULL)
    	return minimum(tree,x->rt);
 	
  	TreeNode *p = x->pt;
  	while (p != NULL && x == p->rt) {
     	x = p;
     	p = p->pt;
  	}

  	return p;
}



TreeNode* predecessor(TTree* tree, TreeNode* x) {
	
	if (x == NULL)
		return NULL;

  	if (x->lt != NULL)
    	return minimum(tree,x->lt);
 	
  	TreeNode *p = x->pt;
  	while (p != NULL && x == p->lt) {
     	x = p;
     	p = p->pt;
  	}

  	return p;
}


// Function that helps balancing the tree
void avlRotateLeft(TTree *tree, TreeNode *x) {

    TreeNode *y = x->rt;
	x->rt = y->lt;

	if (y->lt != NULL)
		y->lt->pt = x;

	y->pt = x->pt;

	if (x == tree->root)
		tree->root = y;
	else {
		if (x->pt->lt == x)
			x->pt->lt = y;
		else
			x->pt->rt = y;
	}
	
	y->lt=x;
	x->pt=y;

	// Update heights
	y->height = MAX(HEIGHT(y->lt), HEIGHT(y->rt)) + 1;
	x->height = MAX(HEIGHT(x->lt), HEIGHT(x->rt)) + 1;
}


// Function that helps balancing the tree
void avlRotateRight(TTree *tree, TreeNode *y) {

    TreeNode *x = y->lt;
	y->lt = x->rt;

	if (x->rt != NULL)
		x->rt->pt = y;

	x->pt = y->pt;

	if (x == tree->root)
		tree->root = y;
	else {
		if (y->pt->lt == y)
			y->pt->lt = x;
		else
			y->pt->rt = x;
	}

	x->rt = y;
	y->pt = x;


	// Update heights
	y->height = MAX(HEIGHT(y->lt), HEIGHT(y->rt)) + 1;
	x->height = MAX(HEIGHT(x->lt), HEIGHT(x->rt)) + 1;
}


// Function that calculates tree balance
int avlGetBalance(TTree *tree,TreeNode *x) {

    if (tree->root == NULL)
        return -1;

    return HEIGHT(x->lt) - HEIGHT(x->rt);
}


void avlFixUp(TTree *tree, TreeNode *y, TreeNode *newnode) {

	do {

		if (y->lt == NULL)
			y->height = y->rt->height + 1;
		else if(y->rt == NULL)
			y->height = y->lt->height + 1;

		if (y->lt != NULL && y->rt != NULL)
			y->height = MAX(y->lt->height, y->rt->height) +1;

		int balance = avlGetBalance(tree,y);

		// Left - Left Case
		if (balance > 1 && tree->compare(newnode->elem, y->lt->elem) < 0) {
			avlRotateRight(tree, y);
			return;
		}

		// Right - Right Case
		if (balance < -1 && tree->compare(newnode->elem, y->rt->elem) > 0) {
			avlRotateLeft(tree, y);
			return;
		}

		// Left - Right Case
		if (balance > 1 && tree->compare(newnode->elem, y->lt->elem) > 0) {
			avlRotateLeft(tree, y->lt);
			avlRotateRight(tree, y);
			return;
		}

		// Right - Left Case
		if (balance < -1 && tree->compare(newnode->elem, y->rt->elem) < 0) {
			avlRotateRight(tree, y->rt);
			avlRotateLeft(tree, y);
			return;
		}

		y = y->pt;	

	} while (y!= NULL);
}


void insert(TTree* tree, void* elem, void* info) {

	// Tree doesn't exist case
	if(!tree)
		return;

	// Creating the new node using auxiliary function
	TreeNode *newnode = createTreeNode(tree,elem,info);

	if (!newnode)
		return;

	// Tree exists, but has 0 nodes case
	if (tree->root == NULL) {
		tree->root = newnode;
		tree->size++;
		return;
	}

	// Node will become eventually NULL, so we keep anc for node parent every time
	TreeNode *node = tree->root;
	TreeNode *anc = tree->root;

 	while (node != NULL) {
 		anc = node;
    	if (tree->compare(newnode->elem,node->elem) < 0) {
        		node = node->lt;
        		continue;
        }
    		
    	else if (tree->compare(newnode->elem,node->elem) > 0) {
        		node = node->rt;
        		continue;
    	}
    	
    	// Equal values are not allowed in BST
    	else if (tree->compare(newnode->elem,node->elem) == 0)
        	return;
 	}

 	newnode->pt = anc;
 	if (tree->compare(newnode->elem,anc->elem) < 0)
 		anc->lt = newnode;
 	else
 		anc->rt = newnode;

 	// New node added - increasing tree size
 	tree->size++;

 	// Balancing the modified tree
 	if (tree->size >= 3)
 		avlFixUp(tree,anc,newnode);

    return;
}

// Standard BST delete + balancing
void delete(TTree* tree, void* elem) {

 //    if (tree->root == NULL)
 //        return;
 // 	int move = 0;
 // 	while (tree->root != NULL) {

 // 		// If we found the right node
	// 	if (tree->compare(elem,tree->root->elem) == 0) {
	// 		if (tree->root->lt == NULL || tree->root->rt == NULL) {

 //            	TreeNode *temp = tree->root->lt ? tree->root->lt : tree->root->rt;
 
 //            	// No child case
 //            	if (temp == NULL) {
 //            		if (move == -1)
 //            			tree->root->pt->lt = NULL;
 //            		else if (move == 1)
	// 					tree->root->pt->rt = NULL;
 //                	free(tree->root);
 //                	break;
 //            	}
 //            	// One child case
 //            	else {
 //            		if (tree->root->pt->lt == tree->root)
 //            			tree->root->pt->lt = temp;
 //            		else
	// 					tree->root->pt->rt = temp;
 //            		free(tree->root);
 //            	}
 //        	}
 //        	else {
 //            	/* Node with two children: Get the inorder
 //            	   successor (smallest in the right subtree). */
 //            	TreeNode *temp = minimum(tree,tree->root->rt);
			 
 //            	// Copy the inorder successor's data to this node
 //            	tree->root->elem = temp->elem;
 
 //            	// Delete the inorder successor
 //            	delete(tree, temp->elem);
 //        	}
	// 		break;
	// 	}

	// 	/* If the key to be deleted is smaller than the
 //           root's key, then it's in left subtree. */
	// 	else if (tree->compare(elem,tree->root->elem) < 0) {
	// 		tree->root = tree->root->lt;
	// 		move = -1;
	// 		continue;
	// 	}

	// 	/* If the key to be deleted is greater than the
 //           root's key, then it's in right subtree. */
	// 	else if (tree->compare(elem,tree->root->elem) > 0) {
	// 		tree->root = tree->root->rt;
	// 		move = 1;
	// 		continue;
	// 	}
	// }

	// // If the tree had only one node then return
 //    if (tree->root == NULL)
 //      return;

 //    // Update height of the current node
 //    tree->root->height = MAX(HEIGHT(tree->root->lt),HEIGHT(tree->root->rt)) + 1;
 
 //    // Get balance factor & check balance
 //    int balance = avlGetBalance(tree,tree->root);
 
 //    // If unbalanced, then fix balance
 //    avlFixUp(tree,temp->pt,temp);
 
    return;
}


void destroyTree(TTree* tree){

	return;
}


#endif /* AVLTREE_H_ */


